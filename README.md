## Activate virtual env
make env
source .venv/bin/activate

## Deactivate virtual env
make clean
deactivate
