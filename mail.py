import boto3
import time


def start_query():
    athena = boto3.client('athena')
    query = "with step0 as (SELECT case when r.replacing_material is null then f.item_code else r.replacing_material end " \
            "as valid_item, cast(f.construction_year as integer) as construction_year FROM " \
            "paas1_pilot_icap_kalmar_spareparts_ad.report_itemfit f left join " \
            "paas1_pilot_icap_cargotec_sap_rawdata_s3.kalmar_replacements r on f.item_code = r.replaced_material where " \
            "f.external_material_group not like '%98' and f.construction_year > '2014'), step1 as (SELECT valid_item, " \
            "max(construction_year) as max_construction_year from step0 group by valid_item), step2 as( select valid_item," \
            "max_construction_year, case when (extract(year from now())-max_construction_year) <=5 then 'G1' when (" \
            "extract(year from now())-max_construction_year) <=10 then 'G2' when (extract(year from now(" \
            "))-max_construction_year) <=15 then 'G3' else 'G4' end as item_generation from step1) select valid_item, " \
            "item_generation from step2 join paas1_pilot_icap_cargotec_sap_rawdata_s3.material on (" \
            "material_number=valid_item) where item_generation != generation "
    s3_output = "s3://awsap-test-athena-ses/"
    database = "paas1-pilot-icap-sap-db"

    start_resp = athena.start_query_execution(
        QueryString=query,
        QueryExecutionContext={
            'Database': database
        },
        ResultConfiguration={
            'OutputLocation': s3_output
        },
    )
    print(start_resp['QueryExecutionId'])
    time.sleep(30)
    return str(start_resp['QueryExecutionId'])


def get_query_results():
    query_execution_id = start_query()
    athena = boto3.client('athena')
    paginator = athena.get_paginator('get_query_results')
    response = athena.get_query_results(
        QueryExecutionId=query_execution_id,
        MaxResults=123
    )

    response_iterator = paginator.paginate(
        QueryExecutionId=query_execution_id,
        PaginationConfig={
            'StartingToken': response['NextToken']
        }
    )
    return response_iterator


def write_file():
    text_file = 'sql_query.txt'
    response_iterator = get_query_results()
    with open(text_file, 'w') as outfile:
        outfile.write("valid_item " + " item_generation" + "\n")
        for iterator in response_iterator:
            for i in iterator['ResultSet']['Rows']:
                print(str(i['Data'][0]['VarCharValue']) + " " + " " + str(i['Data'][1]['VarCharValue']))
                outfile.write(str(i['Data'][0]['VarCharValue']) + "    " + str(i['Data'][1]['VarCharValue']) + "\n")
        return text_file


def send_email():
    ses = boto3.client('ses')
    attachment = str(write_file())
    with open(attachment, 'r') as infile:
        email_response = ses.send_raw_email(
            RawMessage={
                'Data': 'From: ext.Anssi.Rissa@cargotec.com\nTo: ext.Anssi.Rissa@cargotec.com\nCc: ext.Anssi.Rissa@cargotec.com\nSubject: SAP FIP service request - MM update for MARA-ZZGENERATION\nMIME-Version: 1.0\nContent-type: Multipart/Mixed; boundary="NextPart"\n\n--NextPart\nContent-Type: text/plain\n\nThis is the message body.\n\n--NextPart\nContent-Type: text/plain;\nContent-Disposition: infile; filename="sql_query.txt"\n\n' + infile.read() + '\n\n--NextPart--'
            },
        )

    print(email_response)


send_email()
